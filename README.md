# Microscope Image Annotator (mia)

![image](logo.svg)

__Labels microscope images__, __provides a scale__, and __auto-calculates magnification__.

It can also __enhance contrast__, __sharpen__ and __remove colour casts__, and __store metadata__.

https://gitlab.com/the-mia-group/mia

## Examples

| Original                                   | Processed                                   |
| ------------------------------------------ | ------------------------------------------- |
| ![image](examples/philotheca/input.jpg)    | ![image](examples/philotheca/output.jpg)    |
| ![image](examples/gahnia-seed/input.jpg)   | ![image](examples/gahnia-seed/output.jpg)   |
| ![image](examples/dill-seedpods/input.jpg) | ![image](examples/dill-seedpods/output.jpg) |
| ![image](examples/gahnia-glumes/input.jpg) | ![image](examples/gahnia-glumes/output.jpg) |
| ![image](examples/dill/input.jpg)          | ![image](examples/dill/output.jpg)          |

## Workflow

`mia` works well with a workflow like:
 * Capture software (eg. [guvcview](https://guvcview.sourceforge.net/))
 * Focus stacking software (eg. [chimpstackr](https://github.com/noah-peeters/ChimpStackr) or [focus-stack](https://github.com/PetteriAimonen/focus-stack/))
 * `mia`
 * Publish

## Installation

First, download `mia` as a zip file or clone the git repository as follows:

```
$ git clone https://gitlab.com/the-mia-group/mia.git
```

### Distribution-specific setup

#### Ubuntu

```
$ sudo apt-get update
$ sudo apt install php-gd wget aaphoto imagemagick --fix-missing
```

#### Gentoo
```
$ sudo bash
# echo "dev-lang/php avif gd jpeg ttf webp xpm" >>/etc/portage/package.use/php
# emerge -av php wget aaphoto imagemagick
```

#### Other systems

 * Install `php` with the `gd` library, the `freetype` library (TTF support) and whatever image formats you deem required
 * Install `wget` to grab fonts
 * Install `aaphoto` to support contrast enhancement
 * Install `imagemagick` to support sharpening

## Usage

```
$ ./mia
Microscope Image Annotator (mia)

Labels microscope images, provides a scale, and auto-calculates magnification.
It can also enhance contrast, sharpen and remove colour casts, and store metadata.

usage: mia <options>
  --help        -h                             # this help
  --formats     -f                             # List supported input/output file formats
  --input       -i <file>                      # Input in formats:  AVIF BMP JPEG PNG XBM XPM WBMP WEBP 
  --output      -o <file>                      # Output in formats: AVIF BMP JPEG PNG XBM XPM WBMP WEBP 

image information options:
  --microscope  -m  'Microscope name'
  --species     -s  'Species name'
  --subject     -S  'Subject name'
  --feature     -F  'Feature name'
  --date        -d  'YYYY-MM-DD HH:MM:SS'      # otherwise file date or metadata will be used
  --notes       -n  'Free form notes.'
  --magnification      -M 100X                 # otherwise this will be auto-calculated
  --magnification-dpi  -D 96                   # alternate DPI for magnification calculation
  --processing  -p  'Focus stack, negative'.
  --copyright   -C  'CC BY-NC 4.0 @user'

image processing otions:
  --fix                                        # apply recommended image processing (same as -AC)
  --autolevel   -A                             # automatically set levels (uses imagemagick)
  --sharpen     -x                             # sharpens the image (uses imagemagick)
  --contrast    -c                             # stretch contrast (uses aaphoto)
  --colorcast   -C                             # remove color cast (uses FMW's removecolorcast)
  --meta        -m                             # also write metadata to output file (TODO)

output options:
  --view        -V                             # view the output after creating

scale option*:
  --test-scale  -t '<qty> <unit> <pixels>'     # Test or temporary scale, eg. '0.1 mm 256.3'
                                               # means 256.3 pixels represents 0.1mm of reality
named scale preset options:
  --save-scale '<qty> <unit> <pixels> <name>'  # Save a named scale preset, eg. '0.1 mm 256.3 myeg'
  --use-scale   -u <name>                      # Use a named scale preset, eg. 'myeg'
  --list-scales -l                             # List named scale presets
  --rm-scale    -r <name>                      # Remove a named scale preset

* Calibrate using a well-focused image of a thin metal ruler, micrometer scale, or similar item.
  ie. Compare known gradations to: mia -V -i in.jpg -t '1 mm 200' - adjust until perfectly matched.
  Many optical assemblies distort near the edges, so calibrate against the center if possible.
  Save a preset for each commonly used optical path configuration to allow for rapid re-use.
```

## Calibration

Calibrate using a well-focused image of a thin metal ruler, micrometer scale, or similar item.

Here is an example image, showing a thin metal ruler with 0.5mm increments at a relatively high 
magnification.

![image](docs/close-metal-ruler-0.5mm-scale.jpg)

Now you want to compare these known gradations to assumed gradations, and adjust until perfectly
matched.

We can start by running the following command:

```
$ ./mia -V -i docs/close-metal-ruler-0.5mm-scale.jpg -t '1 mm 200'
```

Noting:
 * The `-V` means 'view the output image once generated'
 * The `-i` means 'use this as the input image'
 * The `-t` means 'test or temporary scale specification'
   * The format for the scale is `<qty> <unit> <pixels>` and we specify that '1', 'mm' equals '200' pixels wide.

We find the scale is too small, so we increase the number of pixels per mm to `1700` which
appears very closely aligned.

```
$ ./mia -V -i docs/close-metal-ruler-0.5mm-scale.jpg -t '1 mm 1700'
```

![image](docs/test-1700.jpg)


Since this is correct, we opt to save it as follows.
```
$ ./mia --save-scale '1 mm 1700 my-example'
OK! Saved.
```

Instead of having to remember or recompute the numbers, we can now use it for other images as follows.

```
$ ./mia --use-scale my-example ...
```

Note that many optical assemblies distort near the edges, so calibrate against the center if possible.

It is recommended to save a preset for each commonly used optical path configuration to allow for 
rapid re-use.


## Motivation

When searching for open source microscopy software it was found that the majority of software
relied upon the presence of a GUI and did not readily support batch processing. Furthermore,
it was also found that calibration was often tedious and poorly documented, and that the scales
that were provided had poor legibility, insufficient options or only worked in certain cases.

Our impressions were confirmed by reading [Jambor H, Antonietti A, Alicea B, Audisio TL, Auer S, Bhardwaj V, et al. (2021) *Creating clear and informative image-based figures for scientific publications*. PLoS Biol 19(3): e3001161.](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.3001161) 
which similarly found that many scientific publications lacked clarity and failed to include 
critically associated information and recommended storing scales and other information off 
the image in order to preserve both legibility and information integrity.

We further support the suggestion of [Aubrey Jones, Senior Software Architect at Flagship Biosciences, Inc.](https://flagshipbio.com/optical-magnification-in-the-age-of-digital-microscopy-2/)
to use 'μm/px' as an objective magnification figure in the current era of digital microscopy.
`mia` will automatically calculate and include this figure with all images.

For the vast majority of microscope images, we do not consider that manual labeling, measuring
or other operations are necessarily required. However, particularly when some effort has been
made with respect to focus stacking, etc. then it is nice to be able to properly associate the
relevant metadata with the images taken. This was the purpose of `mia`.

## Design

`mia` is designed to be simple to install and straightforward to operate.

Therefore, it uses `php`'s GD library for image manipulation which itself uses:
 * the `libgd` C image manipulation library
 * the freetype library for TTF font support.
 * specific image format libraries to support formats like AVIF, JPEG, WEBP, XPM, etc.

However, this complexity is generally well packaged under most distributions, so the user does
not have to care how it works.

Compared to a C program `mia` offers the benefit that it is portable and memory safe. It should
also be cross platform in that it should function on arbitrary Unix-like environments.

The program uses only a local settings file to store named scale presets, and a font cache.
